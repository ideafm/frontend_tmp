import config from '../config';

import gulp from 'gulp';
import path from 'path';
import del from 'del';

let g = require('gulp-load-plugins')();

gulp.task('css', () => {
	return gulp.src(config.PATH.css.src)
		.pipe(g.newer(config.PATH.css.dest))
		.pipe(gulp.dest(config.PATH.css.dest));
});

gulp.task('watch:css', () => {
	let w = gulp.watch(config.PATH.css.src, gulp.parallel('css'));
	w.on('unlink', (filepath) => {
		let filePathFromSrc = path.relative(path.resolve(config.PATH.src), filepath);
		let destFilePath = path.resolve(config.PATH.dest, filePathFromSrc);
		del.sync(destFilePath);
	})
});
