import config from '../config';

import gulp from 'gulp';
import path from 'path';
import del from 'del';

let g = require('gulp-load-plugins')();

gulp.task('js:vendor', () => {
	return gulp.src(config.PATH.js.vendorSrc)
		.pipe(gulp.dest(config.PATH.js.vendorDest));
});

gulp.task('watch:js:vendor', () => {
	let w = gulp.watch(config.PATH.js.vendorSrc, gulp.parallel('js:vendor'));
	w.on('unlink', (filepath) => {
		let filePathFromSrc = path.relative(path.resolve(config.PATH.src), filepath);
		let destFilePath = path.resolve(config.PATH.dest, filePathFromSrc);
		del.sync(destFilePath);
	})
});