import config from '../config';

import gulp from 'gulp';
let browserSync = require('browser-sync').create();

gulp.task('serve', (cb) => {
	if(!config.isDevelopment) {
		cb();
		return;
	}
	browserSync.init(config.browserSync.oprions);
	browserSync.watch(config.browserSync.watch).on('change', browserSync.reload);
});





