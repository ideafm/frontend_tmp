import config from '../config';

import gulp from 'gulp';
import path from 'path';
import del from 'del';

gulp.task('fonts', () => {
	return gulp.src(config.PATH.fonts.src)
		.pipe(gulp.dest(config.PATH.fonts.dest));
});

gulp.task('watch:fonts', () => {
	let w = gulp.watch(config.PATH.fonts.src, gulp.parallel('fonts'));
	w.on('unlink', (filepath) => {
		let filePathFromSrc = path.relative(path.resolve(config.PATH.src), filepath);
		let destFilePath = path.resolve(config.PATH.dest, filePathFromSrc);
		del.sync(destFilePath);
	})
});