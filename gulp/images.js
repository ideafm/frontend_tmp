import config from '../config';

import gulp from 'gulp';
import path from 'path';
import del from 'del';
let g = require('gulp-load-plugins')();

gulp.task('images:all', () => {
	return gulp.src(config.PATH.images.srcAll)
		.pipe(g.newer(config.PATH.images.dest))
		.pipe(g.imagemin())
		.pipe(gulp.dest(config.PATH.images.dest))
});

gulp.task('images:svg', () => {
	return gulp.src(config.PATH.images.srcSvg)
		.pipe(g.newer(config.PATH.images.dest))
		.pipe(g.svgo())
		.pipe(gulp.dest(config.PATH.images.dest))
});

gulp.task('images', gulp.parallel('images:all', 'images:svg'));


gulp.task('watch:images', () => {
	let w = gulp.watch(config.PATH.images.src, gulp.parallel('images:all', 'images:svg'));
	w.on('unlink', (filepath) => {
		let filePathFromSrc = path.relative(path.resolve(config.PATH.src), filepath);
		let destFilePath = path.resolve(config.PATH.dest, filePathFromSrc);
		del.sync(destFilePath);
	})
});